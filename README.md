# README
> **The Card Match Game ©**
>
> Created by TJ Olsen | May 2018
>
> App Store: https://apps.apple.com/us/app/the-card-match-game/id1462676215

## Change History

v1.0.0
- First launch
- Added to Apple App Store

## Contribution Guidelines
We welcome public contributions to The Card Match Game. However, we ask that you follow the guidelines set here in this section. Additionally, we ask that you follow the branching guidelines described in the [Branching Methods](#branching-methods) section of this `README`, especially as they relate to [public contributors](#public-contributors).
- Add UI or unit tests where applicable
- Code should be easy to understand with well-named functions, classes, variables, etc.
- Comments should clarify and add insight, not just label large chunks of code

PRs will not be approved until such guidelines are met, and the needed changes will be requested. Other changes may be requested to maintain the integrity of the code base. If reasonable changes are not made before requesting a new review, a PR may be closed and left unmerged.

## Branching Methods
Branches in this repo should follow the patterns explained below. Create your branches off of `develop`.

**NOTE** Do not use the `hotfix/` or `release/` prefixes.

### Defects and Bug Fixes
Use the `bugfix/` prefix, followed by `T<TICKET_NUMBER_HERE>-<TICKET_NAME_HERE>`. E.g. `bugfix/T123-example-ticket-name`.

No feature branch is needed for bug fixes. The PR should point directly to `develop`.

### New Feature Work
First, create a feature branch off of `develop`. Naming for the feature branch should be `feature/F<FEATURE_NUMBER_HERE>-<FEATuRE_NAME_HERE>`. E.g. `feature/F123-example-feature-name`.

For the tickets, branch off the newly created feature branch. Naming of the ticket's branch should be the `feature/` prefix, followed by `F<FEATURE_NUMBER_HERE>/T<TICKET_NUMBER_HERE>-<TICKET_NAME_HERE>`. E.g. `feature/F123/T123-example-ticket-name`.

PRs for the tickets in the feature should all point to the feature branch. Avoid making branches off of unmerged ticket branches. Instead, do your best to finish the ticketed work, get it approved, and merged into the feature branch before making a new branch, which should then come off of `develop`.

### Updating The README
To update the `README`, simply make a branch off of `develop` called `update-readme`. The PR should point to `master`. After merging, remember that the change will also need to be merged back down into `develop`.

#### Public Contributors
Use the prefix `contributor/`, then follow the [Branching Methods](#branching-methods) guidelines. Since public contributors most often will not have a ticket assigned to them from us, do not use ticket or feature numbers, but rather just use descriptive names. For example:
- A bugfix branch from a public contributor would look like `contributor/bugfix/example-ticket-name`
- A feature branch from a contributor would look like `contributor/example-feature-name`
- A ticket branch of a feature from a contributor would look like `contributor/example-feature-name/example-ticket-name`

## Who Do I Talk To
### To Report Issues
To report issues with the app, please create an issue using The Card Match Game's [public issue tracker](https://bitbucket.org/TJOlsenater/thematchinggame_repo/issues).

### Other Inquiries
For other inquiries, please email us at [thecardmatchgame@gmail.com](mailto:thecardmatchgame@gmail.com?subject=[Inquiry])
