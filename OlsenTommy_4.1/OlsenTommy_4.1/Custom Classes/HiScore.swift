//
//  HiScores.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 12/12/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import Foundation
import CoreData

// Custom class to quickly create Hi Scores. Also allows for a hi score to be saved as one single object to Core Data rather than trying to peice together each property later.
class HiScore: NSObject, Codable
{
    //MARK: - Custom Properties
    
    var initials: String
    var time: Double
    var centiSecond: UInt8
    var moves: UInt16
    var date: Date
    
    //MARK: - Overrides
    
    // Custom description property allows for debugging.
    override var description: String
    {
        get
        {
            return "Hi Score:\n\tInitials - \(initials)\n\tTime - \(time)\n\tCentiSecond - \(centiSecond)\n\tMoves - \(moves)\n\tDate - \(date)"
        }
    }
    
    //MARK: - Custom default init
    
    init(initials: String, time: Double, centiSecond: UInt8, moves: UInt16, date: Date)
    {
        self.initials = initials
        self.time = time
        self.centiSecond = centiSecond
        self.moves = moves
        self.date = date
    }
    
    //MARK: - Convenience initializer from an NSMAnagedObject when scores are loaded in from Core Data.
    
    convenience init(managedObject: NSManagedObject)
    {
        self.init(initials: managedObject.value(forKey: AttributeNames.getInitialsAttributeName) as! String, time: managedObject.value(forKey: AttributeNames.getTimeAttributeName) as! Double, centiSecond: managedObject.value(forKey: AttributeNames.getCentiSecondAttributeName) as! UInt8, moves: managedObject.value(forKey: AttributeNames.getMovesAttributeName) as! UInt16, date: managedObject.value(forKey: AttributeNames.getDateAttributeName) as! Date)
    }
}
