//
//  Card.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 12/1/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit

class UICard: UIButton
{
    //MARK: - Custom Properties
    
    var isFaceDown: Bool
    var cardFaceImage: UIImage?
    var cardBackImage: UIImage?
    
    //MARK: - Custom Computed Properties
    
    // If the card is face down, the card back becomes visible.
    //  -- Otherwise, the card face becomes visible.
    var visibleImage: UIImage?
    {
        get
        {
            if isFaceDown
            {
                self.setImage(cardBackImage, for: .normal)
                self.backgroundColor = UIColor.init(displayP3Red: 99.0/255.0, green: 114.0/255.0, blue: 23.0/255.0, alpha: 1)
            }
            else
            {
                self.setImage(cardFaceImage, for: .normal)
                self.backgroundColor = UIColor.white
            }
            
            return self.image(for: .normal)
        }
    }
    
    //MARK: - Coder Initializer
    
    // Allows UIButton elements to be set to UICards in a storyboard.
    required init?(coder aDecoder: NSCoder)
    {
        isFaceDown = true
        cardBackImage = UIImage(named: "CardBack")
        
        super.init(coder: aDecoder)
        
        self.cardFaceImage = self.imageView?.image != nil ? self.imageView?.image : UIImage(named: "Play")
    }
    
    //MARK: - Custom Methods
    
    // Determines which card side is facing the user, and switches the image to the opposite side.
    // Toggles isFaceDown so that card can be properly flipped again later.
    func flipCard()
    {
        isFaceDown = !isFaceDown
        
        imageView?.image = visibleImage
        
        if isFaceDown
        {
            UIView.transition(with: self, duration: 0.4, options: .transitionFlipFromLeft, animations: nil, completion: nil)
        }
        else
        {
            UIView.transition(with: self, duration: 0.4, options: .transitionFlipFromRight, animations: nil, completion: nil)
        }
    }
}
