//
//  SegueIDs.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 11/30/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

// Custom class to handle all segue identifiers within the app.
// The segue identifiers are accessible in every code file.
// This helps to avoid human error when using the ID's while keeping it consistent and easy to use.
class SegueIDs
{
    private static var segueToGame: String = "segueToGame"
    
    static var toGame: String
    {
        get
        {
            return segueToGame
        }
    }
}

// Custom class to handle all cell reuse identifiers within the app.
// The cell reuse identifiers are accessible in every code file.
// This helps to avoid human error when using the ID's while keeping it consistent and easy to use.
class CellReuseIDs
{
    private static var leaderboardCellReuseID: String = "leaderboarCellID_1"
    
    static var getLeaderboardCellReuseID: String
    {
        get
        {
            return leaderboardCellReuseID
        }
    }
}

// Custom class to handle all nib names within the app.
// The nib names are accessible in every code file.
// This helps to avoid human error when using the nib names while keeping it consistent and easy to use.
class NibNames
{
    private static var leaderboardCellNib: String = "LeaderboardTableViewCell"
    
    static var getLeaderboardCellNib: String
    {
        get
        {
            return leaderboardCellNib
        }
    }
}

// Custom class to handle all entity names within the app.
// The entity names are accessible in every code file.
// This helps to avoid human error when using the entity names while keeping it consistent and easy to use.
class EntityNames
{
    private static var hiScoreEntity: String = "HiScoreEntity"
    
    static var getHiScoreEntityName: String
    {
        get
        {
            return hiScoreEntity
        }
    }
}

// Custom class to handle all attribute names within the app.
// The attribute names are accessible in every code file.
// This helps to avoid human error when using the attribute names while keeping it consistent and easy to use.
class AttributeNames
{
    private static var initials: String = "initials"
    private static var time: String = "time"
    private static var centiSecond: String = "centiSecond"
    private static var moves: String = "moves"
    private static var date: String = "date"
    
    static var getInitialsAttributeName: String
    {
        get
        {
            return initials
        }
    }
    
    static var getTimeAttributeName: String
    {
        get
        {
            return time
        }
    }
    
    static var getCentiSecondAttributeName: String
    {
        get
        {
            return centiSecond
        }
    }
    
    static var getMovesAttributeName: String
    {
        get
        {
            return moves
        }
    }
    
    static var getDateAttributeName: String
    {
        get
        {
            return date
        }
    }
}
