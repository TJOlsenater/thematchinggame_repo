//
//  TableViewCell.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 12/12/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit

class LeaderboardTableViewCell: UITableViewCell
{
    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var movesLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
