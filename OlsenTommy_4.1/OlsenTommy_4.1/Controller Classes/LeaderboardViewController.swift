//
//  LeaderboardViewController.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 12/12/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit
import CoreData

class LeaderboardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    //MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - CoreData Building Block Properties
    
    private var managedContext: NSManagedObjectContext!
    private var entityDescription: NSEntityDescription!
    private var hiScoreObjects: [NSManagedObject] = []
    
    //MARK: - Overrides
    
    // This sets the status bar icons to the white icons.
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    // On load:
    //  -- Table view is set up for use.
    //  -- Core Data is set up for use.
    //  -- Saved hi scores are loaded.
    //  -- Saved hi scores are sorted by total time.
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: NibNames.getLeaderboardCellNib, bundle: nil), forCellReuseIdentifier: CellReuseIDs.getLeaderboardCellReuseID)
        
        managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        // Fill out our entityDescription.
        entityDescription = NSEntityDescription.entity(forEntityName: EntityNames.getHiScoreEntityName, in: managedContext)
        
        let fetchRequestNew = NSFetchRequest<NSManagedObject>(entityName: EntityNames.getHiScoreEntityName)
        
        do
        {
            let result = try managedContext.fetch(fetchRequestNew)
            
            for score in result
            {
                hiScoreObjects += [score]
            }
        }
        catch
        {
            
        }
        
        // Sort the hi scores to be orded from fastest time to slowest.
        hiScoreObjects.sort
        {a,b in
            a.value(forKey: AttributeNames.getTimeAttributeName) as! Double + Double((a.value(forKey: AttributeNames.getCentiSecondAttributeName)) as! UInt8) < b.value(forKey: AttributeNames.getTimeAttributeName) as! Double + Double((b.value(forKey: AttributeNames.getCentiSecondAttributeName)) as! UInt8)
        }
    }
    
    //MARK: - Custom Methods
    
    // Returns a string that represents a user-friendly representation of the user's time.
    func getTimeString(timeFormatter: DateComponentsFormatter, time: TimeInterval) -> String
    {
        if let secondsAndMinutes = timeFormatter.string(from: time)
        {
            if time < 10 // Use a 0 in front of seconds to keep padding consistent.
            {
                return "00:0\(secondsAndMinutes)"
            }
            else if time < 60 // Still less than a minute, so add 0's for padding.
            {
                return "00:\(secondsAndMinutes)"
            }
            else // Otherwise, the timeFormatter will take care of padding.
            {
                return secondsAndMinutes
            }
        }

        return "Error"
    }
    
    //MARK: - TableViewCell Methods
    
    // The number of rows shown in the table view will be the number of hi scores saved.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return hiScoreObjects.count
    }
    
    // Formats each cell to represent the cooresponding hi score.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CellReuseIDs.getLeaderboardCellReuseID, for: indexPath) as? LeaderboardTableViewCell
        {
            let currentRow = hiScoreObjects[indexPath.row]
            
            // Create a dateFormatter and timeFormatter to keep how the date and time are displayed to the user consistent.
            let dateFormatter = DateFormatter()
            let timeFormatter = DateComponentsFormatter()
            
            dateFormatter.calendar = Calendar(identifier: .gregorian)
            dateFormatter.setLocalizedDateFormatFromTemplate("dd MMM. yyyy")
            
            timeFormatter.unitsStyle = .positional
            timeFormatter.allowsFractionalUnits = true
            timeFormatter.allowedUnits = [.second, .minute]
            timeFormatter.zeroFormattingBehavior = .default
            
            cell.backgroundColorView.backgroundColor = tableView.backgroundColor
            
            cell.nameLabel.text = (currentRow.value(forKey: AttributeNames.getInitialsAttributeName) as! String)
            
            // Since the time of the game increments by centiseconds, divide the time by 100 then add the left over centiseconds to the end of the string.
            cell.timeLabel.text = "\(getTimeString(timeFormatter: timeFormatter, time: (currentRow.value(forKey: AttributeNames.getTimeAttributeName) as! Double) / 100)):\(currentRow.value(forKey: AttributeNames.getCentiSecondAttributeName) as! UInt8)"
            
            cell.movesLabel.text = (currentRow.value(forKey: AttributeNames.getMovesAttributeName) as! UInt16).description
            cell.dateLabel.text = dateFormatter.string(from: currentRow.value(forKey: AttributeNames.getDateAttributeName) as! Date)
            
            return cell
        }
        else
        {
            print("Error: TableViewCell with identifier of \(CellReuseIDs.getLeaderboardCellReuseID) failed. tableVew(cellForRowAt), LeaderboardViewController.")
            return tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        }
    }
    
    //MARK: - Actions
    
    // Dismisses the leaderboard when back is pressed.
    @IBAction func dismissLeaderboard(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }
}
