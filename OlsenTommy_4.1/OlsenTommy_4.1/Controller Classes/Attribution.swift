//
//  Attribution.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 12/4/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

/*
    Attributions:
 
    All images aquired from Full Sail University.
 
    applause.wave: http://soundbible.com/2087-Audience-Applause.html
    heartbeat.wav: http://soundbible.com/1001-Heartbeat.html
    shuffle.mp3: https://freesound.org/people/ailett/sounds/445031/
    bell.wav: http://soundbible.com/1531-Temple-Bell.html
    swoosh1.wav: http://soundbible.com/2068-Woosh.html
    swoosh2.wav: http://soundbible.com/706-Swoosh-3.html
    swoosh3.wav: http://soundbible.com/682-Swoosh-1.html

 */
