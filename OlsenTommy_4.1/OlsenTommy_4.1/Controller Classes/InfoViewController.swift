//
//  InfoViewController.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 5/4/19.
//  Copyright © 2019 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    //MARK: - Actions

    // Dismisses the info view when back is pressed.
    @IBAction func dismissInfoView(_ sender: UIBarButtonItem)
    {
        
        dismiss(animated: true, completion: nil)
    }
}
