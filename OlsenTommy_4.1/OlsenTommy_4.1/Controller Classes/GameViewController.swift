//
//  GameViewController.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 11/30/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit
import AVFoundation
import CoreData

class GameViewController: UIViewController
{
    //MARK: - Outlets
    
    @IBOutlet var cards: [UICard]!
    @IBOutlet var iPadCards: [UICard]!
    
    @IBOutlet weak var stopButton: UIButton!
    
    // Constraints - Used for animating.
    @IBOutlet var cardConstraintsToAnimate: [NSLayoutConstraint]!
    @IBOutlet var actionBarConstraintsToAnimate: [NSLayoutConstraint]!
    
    // User stats labels.
    @IBOutlet weak var minutesLabel: UILabel!
    @IBOutlet weak var secondsLabel: UILabel!
    @IBOutlet weak var centisecondsLabel: UILabel!
    @IBOutlet weak var numberOfMovesLabel: UILabel!
    
    // Labels shown at win.
    @IBOutlet weak var totalTime: UILabel!
    @IBOutlet weak var totalMoves: UILabel!
    @IBOutlet weak var congratsLabel: UILabel!
    @IBOutlet weak var youWinLabel: UILabel!
    @IBOutlet weak var replayLabel: UILabel!
    @IBOutlet weak var replayButton: UIButton!
    
    //MARK: - CoreData Building Block Properties
    
    private var managedContext: NSManagedObjectContext!
    private var entityDescription: NSEntityDescription!
    private var hiScores: [NSManagedObject] = []
    
    //MARK: - Custom Properties

    var timer: Timer!
    var audioPlayer: AVAudioPlayer!
    
    var isTablet: Bool = false
    
    var oneFlipped: Bool = false
    var time: Int = 0
    var faceImages: [UIImage?] = []
    var firstSelection: UIImage?
    var secondSelection: UIImage?
    var numberOfMoves: Int = 0
    var mp3: String = "mp3"
    var wav: String = "wav"
    
    var currentHiScores: [HiScore] = []
    
    // If the height of the screen is greater than the width, the phone is in portrait mode.
    var isPortrait: Bool
    {
        get
        {
            return UIScreen.main.bounds.height > UIScreen.main.bounds.width ? true : false
        }
    }
    
    // Retuns all playable cards for device depending on size class.
    var playableCardsForDevice: [UICard]
    {
        get
        {
            var cardArray: [UICard] = []
            
            cardArray += cards
            
            if isTablet
            {
                cardArray += iPadCards
            }
            
            return cardArray
        }
    }
    
    //MARK: - Overrides
    
    // Changes the status bar to white icons.
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    // On load:
    //  -- Does initial set up for all cards.
    //  -- If phone is in portrait orientation, the bottom bar is moved off screen to be animated later. Otherwise, the bottom bar will remain where it is and not be animated.
    //  -- Initial set up for Core Data happens.
    //  -- Hi scores saved to Core Data get loaded in.
    override func viewDidLoad()
    {
        super.viewDidLoad()
        stopButton.imageView?.contentMode = .scaleAspectFit
        
        setUpCoreData()
        
        if hiScores.count > 0
        {
            for score in hiScores
            {
                currentHiScores += [HiScore(managedObject: score)]
            }
        }
        
        setUpCards()
        
        if isPortrait
        {
            setConstraints(withAnimation: false, constraints: actionBarConstraintsToAnimate, constant: -UIScreen.main.bounds.height)
        }
    }
    
    // When view appears:
    //  -- If phone is in portrait mode, the action bar is animated up into view from the bottom of the screen. Otherwise, action bar will remain where it is.
    //  -- Deals the cards in with an animation from the top of the screen.
    override func viewDidAppear(_ animated: Bool)
    {
        if isPortrait
        {
            animateActionBar()
        }
        else
        {
            setConstraints(withAnimation: false, constraints: actionBarConstraintsToAnimate, constant: 0)
        }
        
        dealCards()
        playSound(soundName: "shuffle", ofType: mp3)
    }
    
    //MARK: - Custom methods
    
    // An alert is shown that allows user to add their score to the hi scores.
    func addNewHiScore()
    {
        // Create alert that allows user to enter their initials
        createAlert(title: "Hi Score!", message: "Congrats! Enter your initials to save your score.")
    }
    
    // Brings action bar up from bottom of the screen.
    func animateActionBar()
    {
        if isPortrait
        {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveLinear
                , animations:
                {
                    
                    self.setConstraints(withAnimation: true, constraints: self.actionBarConstraintsToAnimate, constant: 0)
            }
                , completion: nil)
        }
    }
    
    // Assigns a face image to each card.
    //  -- Each image in faceImages should have exactly one other image that is the same in the array.
    //  -- cards.count and faceImages.count should be equal. If not, something is wrong and some cards will have nil images.
    func assignCardValues()
    {
        for (card, image) in zip(playableCardsForDevice.shuffled(), faceImages)
        {
            card.cardFaceImage = image
        }
        
        faceImages = []
    }
    
    // This will run if two cards have been selected.
    // Increases number of moves by one and updates the label to express the change.
    //
    // If the two cards are a match:
    //  -- Both cards will be removed from play.
    //  -- First and second selections will be reset to nil.
    //  -- The game will check to see if the user has won with that match.
    //
    // If not a match:
    //  -- First and second selections will be reset to nil.
    //  -- The selected cards will be flipped back over to be face down.
    //  -- Gameplay will continue.
    func checkForMatch()
    {
        numberOfMoves += 1
        numberOfMovesLabel.text = numberOfMoves.description
        
        if firstSelection == secondSelection && firstSelection != nil
        {
            //toggleInteractionForVisibleCards()
            
            delay(delay: 0.6, thenDo:
                {
                    for card in self.playableCardsForDevice
                    {
                        card.isHidden = card.isFaceDown ? false : true
                    }
                    
                    self.clearSelections()
                    self.checkForWin()
                    self.toggleInteractionForVisibleCards()
            })
        }
        else
        {
            //toggleInteractionForVisibleCards()
            
            delay(delay: 0.5, thenDo:
                {
                    for card in self.playableCardsForDevice
                    {
                        if !card.isFaceDown && !card.isHidden
                        {
                            card.flipCard()
                        }
                    }
                    
                    self.playSound(soundName: "swoosh2", ofType: self.wav)
                    self.toggleInteractionForVisibleCards()
                    self.clearSelections()
            })
        }
    }
    
    // If all cards are hidden, the user has won!
    // Stops timer and animates end message into view.
    // If user has a hi score, allows them to add themselves to leaderboard.
    func checkForWin()
    {
        for card in playableCardsForDevice
        {
            if !card.isHidden
            {
                // Not all cards are hidden, user hasn't won, yet.
                // Continue gameplay.
                return
            }
        }
        
        // If any code past this runs, it means a win!
        timer.invalidate()
        playSound(soundName: "applause", ofType: wav)
        
        totalTime.text = "Total Time:\t\(minutesLabel.text!)\(secondsLabel.text!)\(centisecondsLabel.text!)"
        totalMoves.text = "Total Moves:\t\(numberOfMovesLabel.text!)"
        
        self.congratsLabel.isHidden = false
        self.youWinLabel.isHidden = false
        self.totalTime.isHidden = false
        self.totalMoves.isHidden = false
        self.replayLabel.isHidden = false
        self.replayButton.isHidden = false
        
        UIView.animate(withDuration: 1.3, animations:
        {
            self.congratsLabel.alpha = 1
            self.youWinLabel.alpha = 1
            self.totalTime.alpha = 1
            self.totalMoves.alpha = 1
            self.replayLabel.alpha = 1
            self.replayButton.alpha = 1
        })
        
        // If current hi scores is less than 5, we should automatically add player to hi scores.
        if currentHiScores.count < 5
        {
            addNewHiScore()
            return
        }
        
        // If it gets this far, the current hi scores is full.
        
        // Check to see if player's time is faster than any of the hi scores.
        for score in currentHiScores
        {
            if Double(time) < score.time
            {
                addNewHiScore()
                return
            }
        }
    }
    
    // This will eventually run as a result of a user selecting a card.
    // Sets values for first and second selections by checking face value.
    // If two cards have been selected, checks to see if they match.
    func checkSelection(image: UIImage?)
    {
        if image == nil
        {
            print("Error: Card's image is nil. -- checkSelection, GameViewController.")
        }
        
        if firstSelection == nil
        {
            firstSelection = image
            toggleInteractionForVisibleCards()
        }
        else if secondSelection == nil
        {
            secondSelection = image
            checkForMatch()
        }
    }
    
    // Resets first and second selections to nil.
    func clearSelections()
    {
        firstSelection = nil
        secondSelection = nil
    }
    
    // Creates an alert that allows the user to add their score to the hi scores.
    // If the initials are invalid, the user will continue to be prompted to correct it until it is correct.
    func createAlert(title: String, message: String)
    {
        // -- Create an alert and add a text field.
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addTextField
            { (textField) in
                textField.placeholder = "Initials - e.g. 'AAA'"
        }
        
        // -- When OK is pressed, the score will be saved to the Core Data.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:
        { _ in
            let textField = alert.textFields![0]
            
            // Makes sure at least one initial is entered, but only up to 3 (old arcade style.)
            if let characters = textField.text?.count,
                characters < 4,
                characters > 1
            {
                // Sorts the scores by total time.
                self.currentHiScores.sort
                { a, b in
                    a.time + Double(a.centiSecond) < b.time + Double(b.centiSecond)
                }
                
                // If there are already 5 hi scores saved, we need to get rid of the slowest one before adding the new score.
                if self.currentHiScores.count > 4
                {
                    self.currentHiScores.sort
                    { a, b in
                        a.time + Double(a.centiSecond) < b.time + Double(b.centiSecond)
                    }
                    
                    self.currentHiScores.removeLast()
                }
                
                // Add the score to the currentHiScores using the entered initals and game data.
                self.currentHiScores += [HiScore(initials: textField.text!, time: Double(self.time), centiSecond: UInt8(self.centisecondsLabel!.text!)!, moves: UInt16(self.numberOfMoves), date: Date())]
                
                // Clear the Core Data scores so we can overwrite the hi scores with no duplicates.
                self.hiScores = []
                let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: EntityNames.getHiScoreEntityName)
                let request = NSBatchDeleteRequest(fetchRequest: fetch)
                
                do
                {
                    try self.managedContext.execute(request)
                }
                catch
                {
                    print("Error: Failed to execute NSBatchDeleteRequest. createAlert, GameViewController.")
                }
                
                // Set and save all (up to top 5) hi scores to Core Data.
                for (i, score) in self.currentHiScores.enumerated()
                {
                    self.hiScores += [NSManagedObject(entity: self.entityDescription, insertInto: self.managedContext)]
                    
                    self.hiScores[i].setValue(score.initials, forKey: AttributeNames.getInitialsAttributeName)
                    
                    self.hiScores[i].setValue(score.time, forKey: AttributeNames.getTimeAttributeName)
                    
                    self.hiScores[i].setValue(score.centiSecond, forKey: AttributeNames.getCentiSecondAttributeName)
                    
                    self.hiScores[i].setValue(score.moves, forKey: AttributeNames.getMovesAttributeName)
                    
                    self.hiScores[i].setValue(score.date, forKey: AttributeNames.getDateAttributeName)
                }
                
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
            }
            else
            {
                // If we get here, the user has entered invalid initials.
                // (Less than one or more than 3)
                self.createAlert(title: "Hi Score!", message: "Please enter 1 to 3 characters for your initials.")
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // A loop will iterate one half times the number of cards.
    // For each loop, a random number between (including) 0 and 29 will be generated. (This matches the number of face images in the assets.)
    // If that random number has already been selected, a new random number within the same range will again be generated until a unique value is generated.
    // The face image that corresponds to that value will then be added to the faceImages array twice.
    // The result is that there will be 1 image per card in the cards array, and each image will have exactly one match.
    func createPairs()
    {
        var chosenPairs: [Int] = []
        
        for _ in 1...playableCardsForDevice.count/2
        {
            var randomNumber: Int = Int.random(in: 0...29)
            
            while chosenPairs.contains(randomNumber)
            {
                randomNumber = Int.random(in: 0...29)
            }
            
            if !chosenPairs.contains(randomNumber)
            {
                chosenPairs += [randomNumber]
                faceImages += [UIImage(named: "CardFace\(randomNumber)")]
                faceImages += [UIImage(named: "CardFace\(randomNumber)")]
            }
        }
        
        assignCardValues()
    }
    
    // Animates each card into view from the top individually.
    // On the last card, the sneak peek is shown and the game will soon start.
    func dealCards()
    {
        if cardConstraintsToAnimate.count > 1
        {
            UIView.animate(withDuration: 1.3, delay: 0.0, options: .curveLinear,
                           animations:
                {
                    self.cardConstraintsToAnimate[0].constant = 0
                    self.view.layoutIfNeeded()
            }, completion: nil)
            
            delay(delay: 0.1, thenDo:
                {
                    self.cardConstraintsToAnimate.remove(at: 0)
                    self.dealCards()
            })
        }
        else
        {
            UIView.animate(withDuration: 1.3, delay: 0.0, options: .curveLinear,
                           animations:
                {
                    self.cardConstraintsToAnimate[0].constant = 0
                    self.view.layoutIfNeeded()
            },
                           completion:
                {
                    (finished: Bool) in
                    self.showSneakPeek()
            })
        }
    }
    
    // Delays the code for a given amount of seconds before executing desired functionality.
    func delay(delay:Double, thenDo:@escaping ()->())
    {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: thenDo)
    }
    
    // Flips all cards in the cards array.
    func flipAllCards()
    {
        playSound(soundName: "swoosh3", ofType: wav)
        
        for card in playableCardsForDevice
        {
            card.flipCard()
        }
    }
    
    // Starts playing a sound with a giving name and type.
    func playSound(soundName: String, ofType: String)
    {
        if let path = Bundle.main.path(forResource: soundName, ofType: ofType)
        {
            audioPlayer = try? AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
        }
        
        audioPlayer.prepareToPlay()
        audioPlayer.play()
    }
    
    // Sets the constant of each constraint in an array to the same value, either with an animation or without.
    func setConstraints(withAnimation: Bool, constraints: [NSLayoutConstraint], constant: CGFloat)
    {
        if withAnimation
        {
            for constraint in constraints
            {
                constraint.constant = constant
                view.layoutIfNeeded()
            }
        }
        else
        {
            for constraint in constraints
            {
                constraint.constant = constant
            }
        }
    }
    
    // Makes sure:
    //  -- All cards start face down.
    //  -- Images don't get distorted.
    //  -- The corners of the cards are rounded.
    //  -- Players can't flip cards, yet.
    // Sets constraints so cards start off screen.
    // Shuffles the constraints so they will be dealt in randomly, for visual effect only.
    func setUpCards()
    {
        createPairs()
        toggleInteractionForAllCards()
        
        for card in playableCardsForDevice
        {
            card.isFaceDown = true
            card.imageView?.image = card.visibleImage
            card.layer.cornerRadius = 10
            card.imageView?.contentMode = .scaleAspectFit
        }
        
        setConstraints(withAnimation: false, constraints: cardConstraintsToAnimate, constant: -UIScreen.main.bounds.height)
        
        cardConstraintsToAnimate.shuffle()
    }
    
    // Gets Core Data ready to use and pulls down any saved scores already on the device.
    // This lets us compare any new scores to what is already saved.
    func setUpCoreData()
    {
        managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        entityDescription = NSEntityDescription.entity(forEntityName: EntityNames.getHiScoreEntityName, in: managedContext)
        
        let fetchRequestNew = NSFetchRequest<NSManagedObject>(entityName: EntityNames.getHiScoreEntityName)
        
        do
        {
            let result = try managedContext.fetch(fetchRequestNew)
            
            for score in result
            {
                hiScores += [score]
            }
        }
        catch
        {
            print("Error: Fetch request failed. setUpCoreData, GameViewController.")
        }
    }
    
    // All cards are flipped to reveal the face image.
    // Face images remain visible for about 5 seconds.
    // After about 5 seconds, all the cards are then flipped again to be face down and user interaction is enabled.
    func showSneakPeek()
    {
        flipAllCards()
        playSound(soundName: "swoosh3", ofType: wav)
        
        delay(delay: Double(audioPlayer.duration), thenDo:
            {
                self.playSound(soundName: "heartbeat", ofType: self.wav)
        })
        
        delay(delay: 5, thenDo:
            {
                self.flipAllCards()
                self.playSound(soundName: "bell", ofType: self.wav)
                self.toggleInteractionForAllCards()
                self.startGameTimer()
        })
    }
    
    // Starts timer and sets tick equal to one centisecond.
    func startGameTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
    }
    
    // Sets time labels to display player's elapsed time after game starts.
    // Increases elapsed time by one centisecond.
    @objc func tick()
    {
        
        centisecondsLabel.text = time % 100 < 10 ? "0\(time % 100)" : "\(time % 100)"
        secondsLabel.text = (Int(time / 100) % 60) < 10 ? ":0\((Int(time / 100) % 60)):" : ":\((Int(time / 100) % 60)):"
        minutesLabel.text = ((Int(time / 100) / 60) % 99) < 10 ? "0\(((Int(time / 100) / 60) % 99))" : "\(((Int(time / 100) / 60) % 99))"
        time += 1
    }
    
    // Toggles the isUserInteractionEnabled property for all cards in the cards array.
    func toggleInteractionForAllCards()
    {
        for card in playableCardsForDevice
        {
            card.isUserInteractionEnabled = !card.isUserInteractionEnabled
        }
    }
    
    // Toggles the isUserInteractionEnabled property for only visible cards in the cards array.
    func toggleInteractionForVisibleCards()
    {
        for card in playableCardsForDevice
        {
            if !card.isHidden
            {
                card.isUserInteractionEnabled = !card.isUserInteractionEnabled
            }
        }
    }
    
    //MARK: - Actions
    
    // If a card is selected and it is face down, it flips over.
    @IBAction func cardSelected(_ sender: UICard)
    {
        
        if sender.isFaceDown
        {
            
            toggleInteractionForVisibleCards()
            sender.flipCard()
            playSound(soundName: "swoosh1", ofType: wav)
            checkSelection(image: sender.cardFaceImage)
        }
    }
    
    // Hides end message UI elements.
    // Resets the time and number of moves, along with their labels.
    // Gets new cards ready for gameplay again, then deals the new cards.
    @IBAction func replayPressed(_ sender: UIButton)
    {
        congratsLabel.isHidden = true
        youWinLabel.isHidden = true
        totalTime.isHidden = true
        totalMoves.isHidden = true
        replayLabel.isHidden = true
        replayButton.isHidden = true
        
        congratsLabel.alpha = 0
        youWinLabel.alpha = 0
        totalTime.alpha = 0
        totalMoves.alpha = 0
        replayLabel.alpha = 0
        replayButton.alpha = 0
        
        time = 0
        numberOfMoves = 0
        minutesLabel.text = "00"
        secondsLabel.text = ":00:"
        centisecondsLabel.text = "00"
        numberOfMovesLabel.text = "0"
        
        toggleInteractionForAllCards()
        setUpCards()
        
        for card in playableCardsForDevice
        {
            card.isHidden = false
        }
        
        dealCards()
    }
}
