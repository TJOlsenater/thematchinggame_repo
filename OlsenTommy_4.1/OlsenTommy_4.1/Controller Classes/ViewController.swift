//
//  ViewController.swift
//  OlsenTommy_4.1
//
//  Created by Tommy Kent Olsen, Jr on 11/30/18.
//  Copyright © 2018 Tommy Kent Olsen, Jr. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    //MARK: - Outlets
    
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var soundButton: UIButton!
    
    //MARK: - Custom Properties
    
    var soundOn: Bool = true
    
    //MARK: - Overrides
    
    // Changes the status bar to white icons.
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    // When moving to game, sets a variable to determine size class of device.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let game = segue.destination as? GameViewController
        {
            if view.traitCollection.horizontalSizeClass == .regular && view.traitCollection.verticalSizeClass == .regular
            {
                game.isTablet = true
            }
        }
    }
    
    //MARK: - Actions
    
    // When sound icon is pressed, will switch the sound and the icon between on and off.
    @IBAction func toggleSound(_ sender: UIButton)
    {
        if soundOn
        {
            sender.setImage(UIImage(named: "SoundOff"), for: .normal)
            soundOn = !soundOn
        }
        else
        {
            sender.setImage(UIImage(named: "SoundOn"), for: .normal)
            soundOn = !soundOn
        }
    }
    
    // When play is pressed, all UI elements on the screen fade away.
    // When they are completely transparent, a segue to the game view will occur.
    // NOTE: The game view doesn't come in with a default animation. Instead, a custom animation will happen where the action bar will come in from off screen and cards are also dealt in from off the screen.
    @IBAction func playGame(_ sender: UIButton)
    {
        UIView.animate(withDuration: 1.0, delay: 0.2, options: .curveEaseIn,
        animations:
        {
            self.timeView.alpha = 0
            self.logoImageView.alpha = 0
            self.playButton.alpha = 0
            self.soundButton.alpha = 0
        },
        completion:
        {
            (finished: Bool) in
            self.performSegue(withIdentifier: SegueIDs.toGame, sender: self)
        })
    }
    
    // If user quits playing and returns to home screen, this brings the UI elements back into view.
    @IBAction func unwindToViewController(_ segue: UIStoryboardSegue)
    {
        timeView.alpha = 1
        logoImageView.alpha = 1
        playButton.alpha = 1
        soundButton.alpha = 1
    }
}

